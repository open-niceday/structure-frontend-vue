const AuthRouter = [
  {
    path     : '/auth',
    meta     : {routerGuard: false},
    component: () => import (/* webpackChunkName: "auth" */ '@/app/system/view/empty-layout.vue'),
    children : [
      {
        path     : 'login',
        meta     : {routerGuard: false},
        component: () => import(/* webpackChunkName: "auth" */ '@/app/auth/view/login.vue')
      },
    ]
  }
];

export default AuthRouter;
