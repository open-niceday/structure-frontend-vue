const GuideRouter = [
  {
    path     : '/guide',
    redirect : '/guide/button',
    component: () => import (/* webpackChunkName: "guide" */ '@/app/system/view/default-layout.vue'),
    children : [
      {
        path     : 'button',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-button.vue'),
      },
      {
        path     : 'span',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-span.vue'),
      },
      {
        path     : 'cols',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-cols.vue'),
      },
      {
        path     : 'spinner',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-spinner.vue'),
      },
      {
        path     : 'toast',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-toast.vue'),
      },
      {
        path     : 'tabs',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-tabs.vue'),
      },
      {
        path     : 'list',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-list.vue'),
      },
      {
        path     : 'form',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-form.vue'),
      },
      {
        path     : 'file',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-file.vue'),
      },
      {
        path     : 'emoji',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-emoji.vue'),
      },
      {
        path     : 'i18n',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-i18n.vue'),
      },
      {
        path     : 'modal',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-modal.vue'),
      },
      {
        path     : 'editor',
        meta     : {routerGuard: true},
        component: () => import(/* webpackChunkName: "guide" */ '@/app/guide/view/guide-editor.vue'),
      }
    ]
  }
];

export default GuideRouter;
