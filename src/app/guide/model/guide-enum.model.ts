export namespace GUIDE_ENUM {
  export namespace CODE_SPAN {
    export namespace TYPE {
      export enum ENUM {
        TYPE_1 = 'TYPE_1',
        TYPE_2 = 'TYPE_2',
        TYPE_3 = 'TYPE_3',
        TYPE_4 = 'TYPE_4',
      }

      export enum NAME {
        TYPE_1 = 'span-icon icon-type-1',
        TYPE_2 = 'span-icon icon-type-2',
        TYPE_3 = 'span-icon2 icon-type-1',
        TYPE_4 = 'span-icon2 icon-type-2',
      }

      export enum SPAN {
        TYPE_1 = 10,
        TYPE_2 = 11,
        TYPE_3 = 12,
        TYPE_4 = 13,
      }
    }

    export namespace SPAN {
      export enum ENUM {
        SAMPLE_ICON_1 = 'SAMPLE_ICON_1',
        SAMPLE_ICON_2 = 'SAMPLE_ICON_2',
        SAMPLE_ICON_3 = 'SAMPLE_ICON_3',
        SAMPLE_ICON_4 = 'SAMPLE_ICON_4',
        SAMPLE_ICON_5 = 'SAMPLE_ICON_5',
        SAMPLE_ICON_6 = 'SAMPLE_ICON_6',
        SAMPLE_ICON_7 = 'SAMPLE_ICON_7',
        SAMPLE_ICON_8 = 'SAMPLE_ICON_8',
        SAMPLE_ICON_9 = 'SAMPLE_ICON_9',
        SAMPLE_ICON_10 = 'SAMPLE_ICON_10',
        SAMPLE_ICON_11 = 'SAMPLE_ICON_11',
        SAMPLE_ICON_12 = 'SAMPLE_ICON_12',
        SAMPLE_ICON_13 = 'SAMPLE_ICON_13',
        SAMPLE_ICON_14 = 'SAMPLE_ICON_14',
        SAMPLE_ICON_15 = 'SAMPLE_ICON_15',
        SAMPLE_ICON_16 = 'SAMPLE_ICON_16',
        SAMPLE_ICON_17 = 'SAMPLE_ICON_17',
        SAMPLE_ICON_18 = 'SAMPLE_ICON_18',
        SAMPLE_ICON_19 = 'SAMPLE_ICON_19',
        SAMPLE_ICON_20 = 'SAMPLE_ICON_20',
        SAMPLE_ICON_21 = 'SAMPLE_ICON_21',
        SAMPLE_ICON_22 = 'SAMPLE_ICON_22',
        SAMPLE_ICON_23 = 'SAMPLE_ICON_23',
        SAMPLE_ICON_24 = 'SAMPLE_ICON_24',
        SAMPLE_ICON_25 = 'SAMPLE_ICON_25',
        SAMPLE_ICON_26 = 'SAMPLE_ICON_26',
        SAMPLE_ICON_27 = 'SAMPLE_ICON_27',
        SAMPLE_ICON_28 = 'SAMPLE_ICON_28',
        SAMPLE_ICON_29 = 'SAMPLE_ICON_29',
        SAMPLE_ICON_30 = 'SAMPLE_ICON_30',
      }

      export enum NAME {
        SAMPLE_ICON_1 = 'status-01',
        SAMPLE_ICON_2 = 'status-02',
        SAMPLE_ICON_3 = 'status-03',
        SAMPLE_ICON_4 = 'status-04',
        SAMPLE_ICON_5 = 'status-05',
        SAMPLE_ICON_6 = 'status-06',
        SAMPLE_ICON_7 = 'status-07',
        SAMPLE_ICON_8 = 'status-08',
        SAMPLE_ICON_9 = 'status-09',
        SAMPLE_ICON_10 = 'status-10',
        SAMPLE_ICON_11 = 'status-11',
        SAMPLE_ICON_12 = 'status-12',
        SAMPLE_ICON_13 = 'status-13',
        SAMPLE_ICON_14 = 'status-14',
        SAMPLE_ICON_15 = 'status-15',
        SAMPLE_ICON_16 = 'status-16',
        SAMPLE_ICON_17 = 'status-17',
        SAMPLE_ICON_18 = 'status-18',
        SAMPLE_ICON_19 = 'status-19',
        SAMPLE_ICON_20 = 'status-20',
        SAMPLE_ICON_21 = 'status-21',
        SAMPLE_ICON_22 = 'status-22',
        SAMPLE_ICON_23 = 'status-23',
        SAMPLE_ICON_24 = 'status-24',
        SAMPLE_ICON_25 = 'status-25',
        SAMPLE_ICON_26 = 'status-26',
        SAMPLE_ICON_27 = 'status-27',
        SAMPLE_ICON_28 = 'status-28',
        SAMPLE_ICON_29 = 'status-29',
        SAMPLE_ICON_30 = 'status-30',
      }
    }
  }

  export namespace SELECT {
    export enum ENUM {
      TEST_1 = 'TEST_1',
      TEST_2 = 'TEST_2',
    }

    export enum NAME {
      TEST_1 = '테스트-1',
      TEST_2 = '테스트-2',
    }
  }
}
