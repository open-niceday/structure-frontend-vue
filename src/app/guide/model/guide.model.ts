import 'reflect-metadata';
import {Expose, Type}                                                                                                                                     from 'class-transformer';
import {ArrayNotEmpty, IsArray, IsBoolean, IsHexColor, IsInt, IsNotEmpty, IsOptional, IsString, Matches, Max, MaxLength, Min, ValidateIf, ValidateNested} from 'class-validator';

import {Description}         from '@/core/decorator/description.decorator';
import {INPUT_CONFIG}        from '@/app/shared/config/input.config';
import {CODE_EDITOR_CONTENT} from '@/app/guide/config/guide.config';
import {GUIDE_ENUM}          from '@/app/guide/model/guide-enum.model';
import {Pageable}            from '@/app/shared/model/pageable.model';
import {I18n}                from '@/app/shared/model/i18n.model';

export namespace Guide {
  export class Search extends Pageable.Request.Search {
    @Expose() @Description('Search1')
    search1!: string;

    @Expose() @Description('Search2')
    search2!: string;
  }

  export class Page {
    @Expose()
    id!: number;

    @Expose()
    name!: string;

    @Expose()
    url!: string;

    @Expose()
    displayYn: boolean = true;

    @Expose()
    createdBy: string = 'pollra';

    @Expose()
    createdAt: string = '2020-04-24T16:05:19.673';

    constructor(index: number, text: string) {
      this.id = index;
      this.name = `${text}-${index}`;
      this.url = `${text}-${index}`;
    }
  }

  export class Form {
    @Expose() @Description('Input Text')
    @IsString() @IsOptional() @MaxLength(200)
    inputText!: string;

    @Expose() @Description('Input Url')
    @ValidateIf(model => !!model.inputUrl)
    @IsString() @IsOptional() @MaxLength(200) @Matches(INPUT_CONFIG.ABSOLUTE_URL.PATTERN, INPUT_CONFIG.ABSOLUTE_URL.OPTIONS)
    inputUrl!: string;

    @Expose() @Description('Input Relative Url')
    @IsString() @IsNotEmpty() @MaxLength(200) @Matches(INPUT_CONFIG.RELATIVE_URL.PATTERN, INPUT_CONFIG.RELATIVE_URL.OPTIONS)
    inputRelativeUrl!: string;

    @Expose() @Description('Input Number')
    @IsInt() @IsNotEmpty() @Min(0) @Max(600)
    inputNumber!: number;

    @Expose() @Description('Input Color')
    @IsString() @IsNotEmpty() @MaxLength(7) @IsHexColor()
    inputColor!: string;

    @Expose() @Description('Select Enum')
    @IsString() @IsNotEmpty()
    selectEnum!: GUIDE_ENUM.SELECT.ENUM;

    @Expose() @Description('Select Boolean')
    @IsBoolean() @IsNotEmpty()
    selectBoolean!: boolean;

    @Expose() @Description('Date Time Picker')
    @IsString() @IsNotEmpty()
    dateTime!: string;

    @Expose() @Description('Date Time Picker Range Start')
    @IsString() @IsNotEmpty()
    dateTimeRangeStart!: string;

    @Expose() @Description('Date Time Picker Range End')
    @IsString() @IsNotEmpty()
    dateTimeRangeEnd!: string;

    @Expose() @Description('Date Time Second Picker')
    @IsString() @IsNotEmpty()
    dateTimeSecond!: string;

    @Expose() @Description('Date Time Second Picker Range Start')
    @IsString() @IsNotEmpty()
    dateTimeSecondRangeStart!: string;

    @Expose() @Description('Date Time Second Picker Range End')
    @IsString() @IsNotEmpty()
    dateTimeSecondRangeEnd!: string;

    @Expose() @Description('Text Area')
    @IsString() @IsNotEmpty() @MaxLength(200)
    textarea!: string;

    @Expose() @Description('input Text Disable')
    @IsString() @IsNotEmpty() @MaxLength(200)
    inputTextDisable: string = 'input Text Disable';

    @Expose() @Description('Select Enum Disable')
    @IsString() @IsNotEmpty()
    selectEnumDisable: GUIDE_ENUM.SELECT.ENUM = GUIDE_ENUM.SELECT.ENUM.TEST_1;

    @Expose() @Description('Radio')
    @IsString() @IsNotEmpty()
    radio!: string;

    @Expose() @Description('Select')
    @IsString() @IsNotEmpty()
    select!: string;
  }

  export class GuideI18n {
    @Expose() @Description('require')
    @Type(() => I18n.Request.Default)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    require: I18n.Request.Default[] = [];

    @Expose() @Description('optional')
    @Type(() => I18n.Request.Default)
    @IsArray() @IsNotEmpty() @ValidateNested()
    optional: I18n.Request.Default[] = [];

    @Expose() @Description('areaRequire')
    @Type(() => I18n.Request.Default)
    @IsArray() @ArrayNotEmpty() @IsNotEmpty() @ValidateNested()
    areaRequire: I18n.Request.Default[] = [];

    @Expose() @Description('areaOptional')
    @Type(() => I18n.Request.Default)
    @IsArray() @IsNotEmpty() @ValidateNested()
    areaOptional: I18n.Request.Default[] = [];
  }

  export class Editor {
    @Expose() @Description('Editor')
    @IsString() @IsNotEmpty()
    editor: string = CODE_EDITOR_CONTENT;
  }
}
