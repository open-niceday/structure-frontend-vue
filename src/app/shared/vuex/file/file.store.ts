import {createStore, Module} from 'vuex-smart-module';

import {FileState}    from '@/app/shared/vuex/file/file.state';
import {FileGetter}   from '@/app/shared/vuex/file/file.getter';
import {FileMutation} from '@/app/shared/vuex/file/file.mutation';
import {FileAction}   from '@/app/shared/vuex/file/file.action';

const FileContext = new Module({
  state    : FileState,
  getters  : FileGetter,
  mutations: FileMutation,
  actions  : FileAction
});

const Store = createStore(FileContext);

export class FileStore {
  static getInstance() {
    return FileContext.context(Store);
  }
}
