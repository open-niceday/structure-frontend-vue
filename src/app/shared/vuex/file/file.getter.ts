import {Getters} from 'vuex-smart-module';

import {Image}     from '@/app/shared/model/image.model';
import {Mapper}    from '@/core/service/mapper.service';
import {FileState} from '@/app/shared/vuex/file/file.state';

export class FileGetter extends Getters<FileState> {
  private mapper = new Mapper();

  getResultImage(): Image.Response.Result {
    return this.mapper.toObject<Image.Response.Result>(Image.Response.Result, this.state.resultImage);
  }
}
