import {Actions}          from 'vuex-smart-module';
import {ClassConstructor} from 'class-transformer';

import {Enum}              from '@/app/shared/enum/enum';
import {ConditionState}    from '@/app/shared/vuex/condition/condition.state';
import {ConditionGetter}   from '@/app/shared/vuex/condition/condition.getter';
import {ConditionMutation} from '@/app/shared/vuex/condition/condition.mutation';

export class ConditionAction extends Actions<ConditionState, ConditionGetter, ConditionMutation, ConditionAction> {
  refreshCondition() {
    this.mutations.refreshCondition();
  }

  setCondition<T>(params: { key: Enum.CORE.ACTION.CONDITION; type: ClassConstructor<T>; data: T; }) {
    this.mutations.setCondition(params);
  }
}
