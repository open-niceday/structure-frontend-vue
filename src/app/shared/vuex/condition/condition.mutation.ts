import {Mutations}        from 'vuex-smart-module';
import {ClassConstructor} from 'class-transformer';

import {Enum}           from '@/app/shared/enum/enum';
import {Mapper}         from '@/core/service/mapper.service';
import {ConditionState} from '@/app/shared/vuex/condition/condition.state';

export class ConditionMutation extends Mutations<ConditionState> {
  private mapper = new Mapper();

  refreshCondition() {
    this.state.condition = {};
  }

  setCondition<T>(params: { key: Enum.CORE.ACTION.CONDITION; type: ClassConstructor<T>; data: T; }) {
    this.state.condition[params.key] = this.mapper.toObject(params.type, params.data);
  }
}
