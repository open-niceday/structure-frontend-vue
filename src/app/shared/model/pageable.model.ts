import 'reflect-metadata';
import {Expose, Type} from 'class-transformer';

import {Description}                          from '@/core/decorator/description.decorator';
import {DEFAULT_PAGE_SORT, PAGE_DEFAULT_SIZE} from '@/app/shared/config/n-page-info.config';

export namespace Pageable {
  export namespace Request {
    export class Search {
      @Expose()
      page: number = 0;

      @Expose()
      size!: number;

      @Expose()
      sort!: string;

      constructor(options?: Options) {
        this.size = PAGE_DEFAULT_SIZE();
        this.sort = DEFAULT_PAGE_SORT;

        if (!!options) {
          if (options.size !== null && options.size !== undefined) {
            this.size = options.size;
          }

          if (options.sort !== null && options.sort !== undefined) {
            this.sort = options.sort;
          }
        }
      }
    }

    export class Options {
      @Expose()
      size?: number;

      @Expose()
      sort?: string;
    }
  }

  export namespace Response {
    export class Page<T> {
      @Expose()
      first!: boolean;

      @Expose()
      last!: boolean;

      @Expose()
      number!: number;

      @Expose()
      numberOfElements!: number;

      @Expose()
      size!: number;

      @Expose()
      totalElements!: number;

      @Expose()
      totalPages!: number;

      @Expose()
      @Type(() => Pageable)
      pageable!: Pageable;

      @Expose()
      @Type(() => Sort)
      sort!: Sort;

      @Expose() @Description('목록')
      content!: T[];
    }

    export class Pageable {
      @Expose()
      offset!: number;

      @Expose()
      pageNumber!: number;

      @Expose()
      pageSize!: number;

      @Expose()
      paged!: boolean;

      @Expose()
      unpaged!: boolean;

      @Expose()
      @Type(() => Sort)
      sort!: Sort;
    }

    export class Sort {
      @Expose()
      sorted!: boolean;

      @Expose()
      unsorted!: boolean;
    }
  }
}
