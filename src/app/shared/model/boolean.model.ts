import {Shared} from '@/app/shared/model/shared.model';

export namespace Boolean {
  export const COMMON: Shared.Options[] = [
    {value: true, text: '예'},
    {value: false, text: '아니오'}
  ];
}