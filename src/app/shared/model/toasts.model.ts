import 'reflect-metadata';
import {Expose, Type} from 'class-transformer';

import {Enum} from '@/app/shared/enum/enum';

export namespace Toasts {
  export class ValidError {
    @Expose()
    @Type(() => Toasts.ValidForm)
    data!: ValidForm;

    @Expose()
    status!: number;

    @Expose()
    statusText!: string;
  }

  export class CodeError {
    @Expose()
    @Type(() => Toasts.CodeForm)
    data!: CodeForm;

    @Expose()
    status!: number;

    @Expose()
    statusText!: string;
  }

  export class CodeForm {
    @Expose()
    code!: string;

    @Expose()
    message!: string;
  }

  export class ValidForm {
    @Expose()
    status!: number;

    @Expose()
    statusText!: string;

    @Expose()
    error!: string;

    @Expose()
    @Type(() => Toasts.ErrorData)
    errors!: ErrorData[];
  }

  export class ErrorData {
    @Expose()
    defaultMessage!: string;

    @Expose()
    field!: string;

    @Expose()
    rejectedValue!: any;

    @Expose()
    objectName!: string;
  }

  export class Message {
    variant: Enum.CORE.TOASTS.VARIANT = Enum.CORE.TOASTS.VARIANT.warning;
    title!: string;
    contents!: string[];

    constructor(options?: { variant?: Enum.CORE.TOASTS.VARIANT; title?: string; contents?: string[]; }) {
      if (!!options) {
        if (!!options.variant) {
          this.variant = options.variant;
        }

        if (!!options.title) {
          this.title = options.title;
        }

        if (!!options.contents) {
          this.contents = options.contents;
        }
      }
    }
  }
}
