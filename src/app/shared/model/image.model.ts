import 'reflect-metadata';
import {Expose, Type}         from 'class-transformer';
import {IsNotEmpty, IsString} from 'class-validator';

import {Description} from '@/core/decorator/description.decorator';
import {Enum}        from '@/app/shared/enum/enum';

export namespace Image {
  export namespace Request {
    export class Default {
      @Expose() @Description('이미지일련번호')
      @IsString() @IsNotEmpty()
      id!: string;

      @Expose() @Description('파일명')
      name?: string;

      @Expose() @Description('파일경로')
      path?: string;
    }
  }

  export namespace Response {
    export class Result {
      @Expose() @Description('파일타겟')
      target!: string;

      @Expose() @Description('파일타겟')
      action!: Enum.CORE.ACTION.RESULT;

      @Expose() @Description('파일타겟')
      @Type(() => Default)
      image!: Default | null;

      constructor(target?: string, action?: Enum.CORE.ACTION.RESULT) {
        if (!!target) {
          this.target = target;
        }

        if (!!action) {
          this.action = action;
        }
      }
    }

    export class Default {
      @Expose() @Description('이미지일련번호')
      id!: string;

      @Expose() @Description('파일명')
      name!: string;

      @Expose() @Description('파일경로')
      path!: string;
    }
  }
}
