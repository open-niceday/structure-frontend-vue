import 'reflect-metadata';
import {Expose}                                                 from 'class-transformer';
import {IsBoolean, IsNotEmpty, IsOptional, IsString, MaxLength} from 'class-validator';

import {Description} from '@/core/decorator/description.decorator';

export namespace File {
  export namespace Request {
    export class Info {
      @Expose() @Description('파일경로')
      path!: string;

      @Expose() @Description('파일명')
      name!: string;
    }

    export class RequireLine {
      @Expose() @Description('파일명')
      name: string | null = null;

      @Expose() @Description('파일')
      @IsString() @IsNotEmpty() @MaxLength(200)
      path: string | null = null;
    }

    export class OptionLine {
      @Expose() @Description('파일명')
      name: string | null = null;

      @Expose() @Description('파일')
      @IsString() @IsOptional() @MaxLength(200)
      path: string | null = null;
    }

    export class FullRequireFileLine {
      @Expose() @Description('파일명')
      name: string | null = null;

      @Expose() @Description('파일')
      @IsString() @IsNotEmpty() @MaxLength(200)
      path: string | null = null;

      @Expose() @Description('URL')
      @IsString() @IsOptional() @MaxLength(300)
      url!: string;

      @Expose() @Description('새창여부')
      @IsBoolean() @IsOptional()
      newYn!: boolean;
    }

    export class FullOptionLine {
      @Expose() @Description('파일명')
      name: string | null = null;

      @Expose() @Description('파일')
      @IsString() @IsOptional() @MaxLength(200)
      path: string | null = null;

      @Expose() @Description('URL')
      @IsString() @IsOptional() @MaxLength(300)
      url!: string;

      @Expose() @Description('새창여부')
      @IsBoolean() @IsOptional()
      newYn!: boolean;
    }

    export class FullOptionUrlLine {
      @Expose() @Description('파일명')
      name: string | null = null;

      @Expose() @Description('파일')
      @IsString() @IsNotEmpty() @MaxLength(200)
      path: string | null = null;

      @Expose() @Description('URL')
      @IsString() @IsOptional() @MaxLength(300)
      url!: string;

      @Expose() @Description('새창여부')
      @IsBoolean() @IsNotEmpty()
      newYn!: boolean;
    }
  }

  export namespace Response {
    export class Delete {
      @Expose() @Description('파일타겟')
      target!: string;

      @Expose() @Description('파일경로')
      path!: string;

      @Expose() @Description('파일명')
      name!: string;
    }

    export class Upload {
      @Expose() @Description('파일타겟')
      target!: string;

      @Expose() @Description('파일경로')
      path!: string;

      @Expose() @Description('파일명')
      name!: string;
    }

    export class Line {
      @Expose() @Description('이름')
      name!: string;

      @Expose() @Description('경로')
      path!: string;
    }

    export class FullLine {
      @Expose() @Description('파일명')
      name!: string;

      @Expose() @Description('파일')
      path!: string;

      @Expose() @Description('URL')
      url!: string;

      @Expose() @Description('새창여부')
      newYn!: boolean;
    }
  }
}
