import {Expose}                                      from 'class-transformer';
import {IsNotEmpty, IsOptional, IsString, MaxLength} from 'class-validator';

import {Description} from '@/core/decorator/description.decorator';
import {Enum}        from '@/app/shared/enum/enum';

export namespace I18n {
  export namespace Request {
    export class Default {
      @Expose() @Description('언어')
      @IsString() @IsNotEmpty() @MaxLength(50)
      languageType!: Enum.CORE.I18N.LANG.ENUM;

      @Expose() @Description('내용')
      @IsString() @IsNotEmpty() @MaxLength(1000)
      content!: string;

      constructor(languageType: Enum.CORE.I18N.LANG.ENUM, content?: string) {
        this.languageType = languageType;
        if (!!content) {
          this.content = content;
        }
      }
    }
  }

  export namespace Response {
    export class Default {
      @Expose() @Description('영어')
      languageType!: Enum.CORE.I18N.LANG.ENUM;

      @Expose() @Description('한국어')
      content!: string;
    }
  }

  export namespace Input {
    interface CheckI18n {
      EN: string;
      KO: string;
    }

    export class Display implements CheckI18n {
      @Expose() @Description('영어')
      EN!: string;

      @Expose() @Description('한국어')
      KO!: string;
    }

    export class Require implements CheckI18n {
      @Expose() @Description('영어')
      @IsString() @IsNotEmpty() @MaxLength(1000)
      EN!: string;

      @Expose() @Description('한국어')
      @IsString() @IsOptional() @MaxLength(1000)
      KO!: string;
    }

    export class Optional implements CheckI18n {
      @Expose() @Description('영어')
      @IsString() @IsOptional() @MaxLength(1000)
      EN!: string;

      @Expose() @Description('한국어')
      @IsString() @IsOptional() @MaxLength(1000)
      KO!: string;
    }
  }
}
