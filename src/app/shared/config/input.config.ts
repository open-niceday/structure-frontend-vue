import {MESSAGE} from '@/app/shared/config/message.config';

export const INPUT_CONFIG = {
  RELATIVE_URL: {
    PATTERN: (/^\/[a-z0-9._-]*$/g),
    OPTIONS: {message: MESSAGE.WARNING.RELATIVE_URL}
  },
  ABSOLUTE_URL: {
    PATTERN: (/^https?:\/\/(.*)$/),
    OPTIONS: {message: MESSAGE.WARNING.ABSOLUTE_URL}
  }
};
