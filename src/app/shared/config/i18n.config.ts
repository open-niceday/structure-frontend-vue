import {Enum} from '@/app/shared/enum/enum';

export const DEFAULT_LANGUAGE: Enum.CORE.I18N.LANG.ENUM = Enum.CORE.I18N.LANG.ENUM.EN;
export const REQUIRE_LANGUAGES: Enum.CORE.I18N.LANG.ENUM[] = [Enum.CORE.I18N.LANG.ENUM.EN];
