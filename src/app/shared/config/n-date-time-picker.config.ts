const MONTH = ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'];
const LONG_WEEK = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'];
const WEEK = ['일', '월', '화', '수', '목', '금', '토'];

export const N_DATE_TIME_PICKER_LANG = {
  formatLocale   : {
    firstDayOfWeek: 1,
    months        : MONTH,
    monthsShort   : MONTH,
    weekdays      : LONG_WEEK,
    weekdaysShort : WEEK,
    weekdaysMin   : WEEK,
  },
  monthBeforeYear: false,
  yearFormat     : 'YYYY년',
};
