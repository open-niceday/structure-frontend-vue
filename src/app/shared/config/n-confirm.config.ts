export const MESSAGE_OPTIONS = {
  ADD              : {
    title   : '등록 하시겠습니까?',
    yes     : '예',
    no      : '아니오',
    yesClass: 'btn btn-primary btn-sm',
    noClass : 'btn btn-secondary btn-sm',
    yesIcon : 'fa fa-check',
    noIcon  : 'fa fa-times'
  },
  UPDATE           : {
    title   : '수정 하시겠습니까?',
    yes     : '예',
    no      : '아니오',
    yesClass: 'btn btn-primary btn-sm',
    noClass : 'btn btn-secondary btn-sm',
    yesIcon : 'fa fa-check',
    noIcon  : 'fa fa-times'
  },
  DELETE           : {
    title   : '삭제 하시겠습니까?',
    yes     : '예',
    no      : '아니오',
    yesClass: 'btn btn-danger btn-sm',
    noClass : 'btn btn-secondary btn-sm',
    yesIcon : 'fa fa-trash',
    noIcon  : 'fa fa-times'
  },
  APPLY            : {
    title   : '적용 하시겠습니까?',
    yes     : '예',
    no      : '아니오',
    yesClass: 'btn btn-success btn-sm',
    noClass : 'btn btn-secondary btn-sm',
    yesIcon : 'fa fa-check',
    noIcon  : 'fa fa-times'
  },
  DIRECT_APPLY     : {
    title   : '즉시적용 하시겠습니까?',
    yes     : '예',
    no      : '아니오',
    yesClass: 'btn btn-primary btn-sm',
    noClass : 'btn btn-secondary btn-sm',
    yesIcon : 'fa fa-check',
    noIcon  : 'fa fa-times'
  },
  CHANGE_DISPLAY_YN: {
    title   : '사용여부를 변경 하시겠습니까?',
    yes     : '예',
    no      : '아니오',
    yesClass: 'btn btn-primary btn-sm',
    noClass : 'btn btn-secondary btn-sm',
    yesIcon : 'fa fa-check',
    noIcon  : 'fa fa-times'
  }
};
