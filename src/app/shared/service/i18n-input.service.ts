import {ValidationError, Validator} from 'class-validator';

import {DEFAULT_LANGUAGE, REQUIRE_LANGUAGES} from '@/app/shared/config/i18n.config';
import {MESSAGE}                             from '@/app/shared/config/message.config';
import {Enum}                                from '@/app/shared/enum/enum';
import {I18n}                                from '@/app/shared/model/i18n.model';
import {Layout}                              from '@/app/system/model/layout.model';
import StorageService                        from '@/app/shared/service/storage.service';

export default class I18nInputService {
  private validator = new Validator();
  private storageService = new StorageService();

  getState(obj: any): boolean | null {
    let returnValue: boolean | null = null;

    if (!this.isEmpty(obj)) {
      returnValue = true;

      const errors: ValidationError[] = this.validator.validateSync(obj);

      if (!!errors && errors.length > 0) {
        returnValue = false;
      }
    }

    return returnValue;
  }

  isEmpty(params: any): boolean {
    let returnValue = true;

    if (!!params &&
      (
        (params[Enum.CORE.I18N.LANG.ENUM.EN] !== null && params[Enum.CORE.I18N.LANG.ENUM.EN] !== undefined) ||
        (params[Enum.CORE.I18N.LANG.ENUM.KO] !== null && params[Enum.CORE.I18N.LANG.ENUM.KO] !== undefined)
      )
    ) {
      returnValue = false;
    }

    return returnValue;
  }

  getMessage(obj: any): string {
    let returnValue: string = '';

    if (!this.isEmpty(obj)) {
      const errors: ValidationError[] = this.validator.validateSync(obj);
      errors.forEach((error: ValidationError) => {
        const property = Enum.CORE.I18N.LANG.NAME[error.property];

        if (!!property) {
          if (!!returnValue) {
            returnValue = returnValue + '  ';
          }

          returnValue = returnValue + MESSAGE.WARNING.VALID_LANG_INPUT(property);
        }
      });
    }

    return returnValue;
  }

  getLanguage(): Enum.CORE.I18N.LANG.ENUM {
    let returnValue: Enum.CORE.I18N.LANG.ENUM = DEFAULT_LANGUAGE;

    const setting: Layout.SystemSetting = this.storageService.getLocalStorage(Enum.CORE.STORAGE.KEY.LIVE_SYSTEM, Enum.CORE.STORAGE.TYPE.OBJECT, Layout.SystemSetting);
    if (!!setting && !!setting.lang) {
      returnValue = setting.lang;
    }

    return returnValue;
  }

  getContent(models: I18n.Request.Default[]): string {
    let returnValue: string = '';
    const lang: Enum.CORE.I18N.LANG.ENUM = this.getLanguage();

    models.forEach((model: I18n.Request.Default) => {
      if (model.languageType === lang) {
        returnValue = model.content;
      }
    });

    return returnValue;
  }

  isExist(languageType: Enum.CORE.I18N.LANG.ENUM, targets: I18n.Request.Default[]): boolean {
    let returnValue: boolean = false;

    targets.forEach((target: I18n.Request.Default) => {
      if (languageType === target.languageType) {
        returnValue = true;
      }
    });

    return returnValue;
  }

  getExistIndex(languageType: Enum.CORE.I18N.LANG.ENUM, targets: I18n.Request.Default[]): number | null {
    let returnValue: number | null = null;

    targets.forEach((target: I18n.Request.Default, index: number) => {
      if (languageType === target.languageType) {
        returnValue = index;
      }
    });

    return returnValue;
  }

  isLanguageRequire(lang: Enum.CORE.I18N.LANG.ENUM): boolean {
    let returnValue: boolean = false;

    if (REQUIRE_LANGUAGES.indexOf(lang) >= 0) {
      returnValue = true;
    }

    return returnValue;
  }
}
