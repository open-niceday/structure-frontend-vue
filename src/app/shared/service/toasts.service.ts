import _                 from 'lodash';
import Vue, {VNode}      from 'vue';
import {ValidationError} from 'class-validator';

import {MESSAGE} from '@/app/shared/config/message.config';

export default class ToastsService extends Vue {
  getTitle(title: string): VNode {
    const h = this.$createElement;
    return h(
      'div',
      {class: ['d-flex', 'flex-grow-1', 'align-items-baseline', 'mr-2']},
      [
        h('strong', {class: 'mr-2'}, title),
        h('small', {class: 'ml-auto text-italics'}, ''),
      ],
    );
  }

  getContents(contents: string[]): VNode {
    const h = this.$createElement;
    const nodes: VNode[] = [];
    contents.forEach((content: string) => {
      nodes.push(h('li', {}, content));
    });

    return h('ul', {class: ['text-left', 'mb-0', 'ml-3']}, nodes);
  }

  validate(err: any) {
    if (!_.isEmpty(err.constraints) && !!err.constraints.$description && !!err.target.$description[err.property]) {
      err.constraints.$description = err.target.$description[err.property];
    }

    if (!_.isEmpty(err.children)) {
      err.children.forEach((e: any) => {
        this.validate(e);
      });
    }
  }

  setDescription(errors: ValidationError[], messages: string[], parentDescription?: any[]): string[] {
    errors.forEach((error: ValidationError) => {
      let description: string = '';

      if (!!error.target && !!error.target[`$description`] && !!error.target[`$description`][error.property]) {
        description = error.target[`$description`][error.property];
      }

      const descriptions: any = !!parentDescription ? this.$util.arrayAssign(parentDescription) : [];
      if (!!description) {
        if (!!descriptions) {
          descriptions.push(description);
        }
      }

      if (!!error.children && error.children.length > 0) {
        messages = this.setDescription(error.children, messages, descriptions);
      }

      if (!!error.constraints) {
        const message: string[] = [];
        _.keys(error.constraints).forEach((key: string) => {
          if (key !== `$description`) {
            if (!!MESSAGE.VALIDATOR[key]) {
              message.push(MESSAGE.VALIDATOR[key]);
            } else {
              message.push(!!error.constraints ? error.constraints[key] : '');
            }
          }
        });

        message.forEach((value: string) => {
          messages.push(`${this.setMessage(descriptions)}: ${value}`);
        });
      }
    });

    return messages;
  }

  setMessage(descriptions: any[]): string {
    let returnValue: string = '';

    descriptions.forEach((description: string, index: number) => {
      if (index === 0) {
        returnValue = description;
      } else {
        returnValue = returnValue + '-' + description;

      }
    });

    return returnValue;
  }
}
