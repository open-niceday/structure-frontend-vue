import _      from 'lodash';
import moment from 'moment-timezone';

import {Enum} from '@/app/shared/enum/enum';

export default class UtilService {
  safeCall(parent: any, key: string) {
    return _.get(parent, key);
  }

  objectAssign<T>(obj: T): T {
    return Object.assign({}, obj);
  }

  arrayAssign<T>(obj: T): T {
    return Object.assign([], obj);
  }

  jsonCopy<T>(obj: T): T {
    return JSON.parse(JSON.stringify(obj));
  }

  dateToFormatDate(date: Date | string, format: Enum.CORE.FORM.DATE_FORMAT): string {
    return moment(date).format(format);
  }

  isEmpty(checker: any): boolean {
    let flag = false;

    if (_.isPlainObject(checker)) {
      _.values(checker).forEach((value) => {
        if (this.isEmpty(value)) {
          flag = true;
        }
      });
    } else if (_.isArray(checker)) {
      if (checker.length > 0) {
        checker.forEach((value: any) => {
          if (this.isEmpty(value)) {
            flag = true;
          }
        });
      } else {
        flag = true;
      }
    } else if (_.isNumber(checker)) {
      if (_.isNaN(checker) || _.isNull(checker)) {
        flag = true;
      }
    } else if (_.isString(checker)) {
      if (_.isEmpty(checker)) {
        flag = true;
      }
    } else if (_.isBoolean(checker)) {
      if (_.isNull(checker)) {
        flag = true;
      }
    } else {
      if (_.isEmpty(checker)) {
        flag = true;
      }
    }

    return flag;
  }

  isEmptyPlane(checker: any): boolean {
    let flag = false;
    if (_.isArray(checker)) {
      checker.forEach((value: any) => {
        if (_.isEmpty(value)) {
          flag = true;
        }
      });
    } else {
      if (_.isNumber(checker)) {
        if (_.isNaN(checker) || _.isNull(checker)) {
          flag = true;
        }
      } else {
        if (_.isEmpty(checker)) {
          flag = true;
        }
      }
    }

    return flag;
  }

  getRandomKey(prefix: string = '', length: number = 32): string {
    let result: string = '';
    const characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength: number = characters.length;

    if (!!prefix) {
      prefix = `${prefix}-`;
    }

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return `${prefix}${result}`;
  }

  isLineBreakLimit(value: string, limit: number) {
    let returnValue: boolean = false;

    if (!!value) {
      const match = value.match(/(\n)/g);
      if (!!match && match.length >= limit) {
        returnValue = true;
      }
    }

    return returnValue;
  }

  getIdNameToDisplay(id: string | number | null | undefined, name: string | null | undefined): string {
    let returnValue = '';

    if (!this.isEmpty(id) && !this.isEmpty(name)) {
      returnValue = `(${id}) ${name}`;
    }

    return returnValue;
  }

  fillZero(width: number, str: string): string {
    return str.length >= width ? str : new Array(width - str.length + 1).join('0') + str;
  }
}
