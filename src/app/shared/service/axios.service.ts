import Axios, {AxiosRequestConfig, AxiosResponse, AxiosInstance, AxiosError} from 'axios';
import qs                                                                    from 'qs';

import {Enum}         from '@/app/shared/enum/enum';
import {Broadcast}    from '@/core/service/broadcast.service';
import StorageService from '@/app/shared/service/storage.service';

export default class AxiosService {
  private axios: AxiosInstance;
  private broadcast: Broadcast = new Broadcast();
  private storageService = new StorageService();
  private tokenCreates: { baseUrl: string; url: string; method: string; }[] = [
    {baseUrl: `${process.env.VUE_APP_AUTH_URL}`, url: '/oauth/token', method: 'post'}
  ];

  private tokenNotAllows: { baseUrl: string; url: string; method: string; }[] = [
    {baseUrl: `${process.env.VUE_APP_AUTH_URL}/api`, url: '/me', method: 'get'},
  ];

  constructor(baseURL: string) {
    const timeout = 1000 * 60;
    this.axios = Axios.create({
      baseURL,
      timeout,
      paramsSerializer: (params) => qs.stringify(params, {encode: false, allowDots: true}),
    });

    this.axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        if (this.isAuthorization(this.tokenCreates, baseURL, config)) {
          config.headers[`Authorization`] = `Basic ${process.env.VUE_APP_AUTH_HEADER}`;
          config.headers[`Content-Type`] = process.env.VUE_APP_AUTH_CONTENT_TYPE;
        } else if (!this.isAuthorization(this.tokenNotAllows, baseURL, config)) {
          const authToken = this.storageService.getLocalStorage(Enum.CORE.STORAGE.KEY.LIVE_AUTH_TOKEN, Enum.CORE.STORAGE.TYPE.OBJECT);
          if (!!authToken) {
            config.headers[`Authorization`] = `Bearer ${authToken[`access_token`]}`;
          }
        }

        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );

    this.axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        this.broadcast.onDefaultSpinner(true);
        return config;
      },
      (error) => {
        this.broadcast.onError(error);
        return Promise.reject(error);
      }
    );

    this.axios.interceptors.response.use(
      (response: AxiosResponse) => {
        this.broadcast.onDefaultSpinner(false);
        return response;
      },
      (error: AxiosError) => {
        this.broadcast.onError(error);
        return Promise.reject(error);
      }
    );
  }

  get(url: string, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.get(url, options);
  }

  delete(url: string, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.delete(url, options);
  }

  post(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.post(url, params, options);
  }

  put(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.put(url, params, options);
  }

  patch(url: string, params?: any, options?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.axios.patch(url, params, options);
  }

  private isAuthorization(compare: { baseUrl: string; url: string; method: string; }[], baseUrl: string, config: AxiosRequestConfig): boolean {
    let returnValue: boolean = false;

    compare.forEach((tokenNotAllow: { baseUrl: string; url: string; method: string; }) => {
      if (baseUrl === tokenNotAllow.baseUrl && config.url === tokenNotAllow.url && config.method === tokenNotAllow.method) {
        returnValue = true;
      }
    });

    return returnValue;
  }
}
