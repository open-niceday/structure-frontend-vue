import {Enum}      from '@/app/shared/enum/enum';
import UtilService from '@/app/shared/service/util.service';

export default class NDateTimePickerService {
  private util = new UtilService();

  setDate(date: string | null, pattern: Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME | Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME_SECOND): string | null {
    let returnValue: string | null = null;

    if (!!date) {
      switch (pattern) {
        case Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME:
          returnValue = this.util.dateToFormatDate(date, Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME);
          break;
        case Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME_SECOND:
          returnValue = this.util.dateToFormatDate(date, Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME_SECOND);
          break;
      }
    }

    return returnValue;
  }

  getDate(date: string | null, pattern: Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME | Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME_SECOND): string | null {
    let returnValue: string | null = null;

    if (!!date) {
      switch (pattern) {
        case Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME:
          returnValue = this.util.dateToFormatDate(date, Enum.CORE.FORM.DATE_FORMAT.RETURN_DATE_TIME);
          break;
        case Enum.CORE.FORM.DATE_FORMAT.DISPLAY_DATE_TIME_SECOND:
          returnValue = this.util.dateToFormatDate(date, Enum.CORE.FORM.DATE_FORMAT.RETURN_DATE_TIME_SECOND);
          break;
      }
    }

    return returnValue;
  }
}
