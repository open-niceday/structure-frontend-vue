import NotFound from '@/app/system/view/not-found.vue';

const SharedRouter = [
  {
    path    : '/',
    redirect: '/auth/login',
  },
  {
    path     : '/not-found',
    meta     : {routerGuard: false},
    component: NotFound,
  },
  {
    path    : '/sample',
    redirect: '/dashboard',
  },
  {
    path    : '*',
    redirect: '/not-found',
  }
];

export default SharedRouter;
