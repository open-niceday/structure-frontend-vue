export namespace StorageEnum {
  export enum KEY {
    LIVE_ME = 'LIVE_ME',
    LIVE_AUTH_TOKEN = 'LIVE_AUTH_TOKEN',
    LIVE_MENU = 'LIVE_MENU',
    LIVE_SYSTEM = 'LIVE_SYSTEM'
  }

  export enum TYPE {
    STRING = 'string',
    NUMBER = 'number',
    BOOLEAN = 'boolean',
    OBJECT = 'object',
    ARRAY = 'array',
  }
}
