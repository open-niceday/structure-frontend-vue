export namespace ToastsEnum {
  export enum VARIANT {
    primary = 'primary',
    secondary = 'secondary',
    success = 'success',
    warning = 'warning',
    danger = 'danger',
    info = 'info',
    light = 'light',
    dark = 'dark',
  }
}
