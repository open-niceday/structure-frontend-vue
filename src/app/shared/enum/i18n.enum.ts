export namespace I18nEnum {
  export enum MODEL {
    REQUIRE = 'REQUIRE',
    OPTIONAL = 'OPTIONAL'
  }

  export namespace LANG {
    export enum ENUM {
      EN = 'EN',
      KO = 'KO',
    }

    export enum NAME {
      EN = '영어',
      KO = '한국어',
    }
  }
}
