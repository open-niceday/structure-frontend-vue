export namespace FormEnum {
  export enum INPUT_TYPE {
    TEXT = 'text',
    NUMBER = 'number',
    SEARCH = 'search',
    PASSWORD = 'password',
  }

  export enum DATE_FORMAT {
    RETURN_DATE_TIME = 'YYYY-MM-DDTHH:mm',
    RETURN_DATE_TIME_SECOND = 'YYYY-MM-DDTHH:mm:ss',

    DISPLAY_DATE_TIME = 'YYYY-MM-DD HH:mm',
    DISPLAY_DATE_TIME_SECOND = 'YYYY-MM-DD HH:mm:ss'
  }

  export enum OPTIONS {
    ALL = 'ALL',
    SELECT = 'SELECT',
    NOT_USE = 'NOT_USE'
  }
}
