import {I18nEnum}    from '@/app/shared/enum/i18n.enum';
import {StorageEnum} from '@/app/shared/enum/storage.enum';
import {FormEnum}    from '@/app/shared/enum/form.enum';
import {ToastsEnum}  from '@/app/shared/enum/toasts.enum';
import {ActionEnum}  from '@/app/shared/enum/action.enum';

export namespace Enum {
  export namespace CORE {
    export import I18N = I18nEnum;
    export import STORAGE = StorageEnum;
    export import FORM = FormEnum;
    export import TOASTS = ToastsEnum;
    export import ACTION = ActionEnum;

    export enum ENV {
      LOCAL = 'local',
      DEV = 'development',
      PROD = 'production'
    }
  }
}
