import {GUIDE, MENU} from '@/app/system/config/left-menu.config';
import {Enum}        from '@/app/shared/enum/enum';
import {Layout}      from '@/app/system/model/layout.model';

export default class LayoutService {
  getMenu(): Layout.LeftMenu[] {
    let menus: Layout.LeftMenu[] = [...MENU];

    if (
      process.env.NODE_ENV === Enum.CORE.ENV.LOCAL ||
      process.env.NODE_ENV === Enum.CORE.ENV.DEV ||
      process.env.NODE_ENV === Enum.CORE.ENV.PROD
    ) {
      menus = [...menus, ...GUIDE];
    }

    return menus;
  }
}
