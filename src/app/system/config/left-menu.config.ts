import {Layout} from '@/app/system/model/layout.model';

export const MENU: Layout.LeftMenu[] = [
  {
    title: '대시보드',
    href : '/dashboard',
    icon : 'fas fa-home'
  },
  {
    title: '샘플',
    value: '/sample',
    icon : 'fas fa-broadcast-tower',
    child: [
      {
        title: '채널',
        href: '/sample/album',
      },
    ]
  }
];

export const GUIDE: Layout.LeftMenu[] = [
  {
    title: '가이드',
    value: '/guide',
    icon : 'fas fa-palette',
    child: [
      {
        title: 'Button',
        href : '/guide/button'
      },
      {
        title: 'Span',
        href : '/guide/span'
      },
      {
        title: 'Cols',
        href : '/guide/cols'
      },
      {
        title: 'Spinner',
        href : '/guide/spinner'
      },
      {
        title: 'Toast',
        href : '/guide/toast'
      },
      {
        title: 'Tabs',
        href : '/guide/tabs'
      },
      {
        title: 'List',
        href : '/guide/list'
      },
      {
        title: 'Form',
        href : '/guide/form'
      },
      {
        title: 'File',
        href : '/guide/file'
      },
      {
        title: 'Emoji',
        href : '/guide/emoji'
      },
      {
        title: 'I18n',
        href : '/guide/i18n'
      },
      {
        title: 'Modal',
        href : '/guide/modal'
      },
      {
        title: 'Editor',
        href : '/guide/editor'
      },
    ]
  },
];
