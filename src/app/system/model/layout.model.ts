import {Expose, Type} from 'class-transformer';

import {Description}  from '@/core/decorator/description.decorator';
import {Enum}         from '@/app/shared/enum/enum';
import StorageService from '@/app/shared/service/storage.service';

const storageService = new StorageService();

export namespace Layout {
  export class LeftMenu {
    @Expose() @Description('페이지번호')
    title!: string;

    @Expose() @Description('페이지크기')
    href?: string;

    @Expose() @Description('페이지크기')
    value?: string;

    @Expose() @Description('페이지크기')
    icon?: string | null;

    @Expose() @Description('전체페이지수')
    @Type(() => LeftMenu)
    child?: LeftMenu[];
  }

  export class SystemSetting {
    @Expose() @Description('페이지사이즈')
    size!: number;

    @Expose() @Description('기본표시언어')
    lang!: Enum.CORE.I18N.LANG.ENUM;

    constructor() {
      const system = storageService.getLocalStorage(Enum.CORE.STORAGE.KEY.LIVE_SYSTEM, Enum.CORE.STORAGE.TYPE.OBJECT);
      if (!!system && !!system.size) {
        this.size = system.size;
      }

      if (!!system && !!system.lang) {
        this.lang = system.lang;
      }
    }
  }
}

