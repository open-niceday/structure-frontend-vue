import {Shared}      from '@/app/shared/model/shared.model';
import {SampleAlbum} from '@/app/sample/album/model/sample-album.model';

export class SampleAlbumState {
  list: SampleAlbum.Response.FindAll[] = [];
  one: SampleAlbum.Response.FindOne = new SampleAlbum.Response.FindOne();
  result: Shared.Response.ActionResult = new Shared.Response.ActionResult();
}
