import {Mutations} from 'vuex-smart-module';

import {Shared}           from '@/app/shared/model/shared.model';
import {SampleAlbum}      from '@/app/sample/album/model/sample-album.model';
import {SampleAlbumState} from '@/app/sample/album/vuex/sample-album.state';
import {Mapper}           from '@/core/service/mapper.service';

export class SampleAlbumMutation extends Mutations<SampleAlbumState> {
  private mapper = new Mapper();

  setList(list: SampleAlbum.Response.FindAll[]) {
    this.state.list = this.mapper.toArray<SampleAlbum.Response.FindAll>(SampleAlbum.Response.FindAll, list);
  }

  setOne(one: SampleAlbum.Response.FindOne) {
    this.state.one = this.mapper.toObject<SampleAlbum.Response.FindOne>(SampleAlbum.Response.FindOne, one);
  }

  setResult(result: Shared.Response.ActionResult) {
    this.state.result = this.mapper.toObject<Shared.Response.ActionResult>(Shared.Response.ActionResult, result);
  }
}
