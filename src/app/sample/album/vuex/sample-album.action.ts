import {AxiosResponse} from 'axios';
import {Actions}       from 'vuex-smart-module';

import {Validate}            from '@/core/decorator/validate.decorator';
import {Enum}                from '@/app/shared/enum/enum';
import {SampleAlbum}         from '@/app/sample/album/model/sample-album.model';
import {SampleAlbumState}    from '@/app/sample/album/vuex/sample-album.state';
import {SampleAlbumGetter}   from '@/app/sample/album/vuex/sample-album.getter';
import {SampleAlbumMutation} from '@/app/sample/album/vuex/sample-album.mutation';
import AxiosService          from '@/app/shared/service/axios.service';

export class SampleAlbumAction extends Actions<SampleAlbumState, SampleAlbumGetter, SampleAlbumMutation, SampleAlbumAction> {
  private http = new AxiosService(`${process.env.VUE_APP_API_URL}`);

  getList(params: SampleAlbum.Request.Search) {
    this.http.get(
      `/albums`,
      {params}
    ).then(
      (response: AxiosResponse<SampleAlbum.Response.FindAll[]>) =>
        this.mutations.setList(response.data)
    );
  }

  getOne(id: string) {
    this.http.get(
      `/albums/${id}`
    ).then(
      (response: AxiosResponse<SampleAlbum.Response.FindOne>) =>
        this.mutations.setOne(response.data)
    );
  }

  @Validate
  add(params: SampleAlbum.Request.Add) {
    this.http.post(
      `/albums`,
      params
    ).then(
      (response: AxiosResponse<SampleAlbum.Response.FindOne>) =>
        this.mutations.setResult({action: Enum.CORE.ACTION.RESULT.ADD, data: response.data})
    );
  }

  @Validate
  update(params: SampleAlbum.Request.Modify) {
    this.http.put(
      `/albums/${params.id}`,
      params
    ).then(
      (response: AxiosResponse<SampleAlbum.Response.FindOne>) =>
        this.mutations.setResult({action: Enum.CORE.ACTION.RESULT.UPDATE, data: response.data})
    );
  }

  delete(id: string) {
    this.http.delete(
      `/albums/${id}`
    ).then(
      (response: AxiosResponse<number>) =>
        this.mutations.setResult({action: Enum.CORE.ACTION.RESULT.DELETE, data: response.data})
    );
  }
}
