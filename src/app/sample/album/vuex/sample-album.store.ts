import {createStore, Module} from 'vuex-smart-module';

import {SampleAlbumState}    from '@/app/sample/album/vuex/sample-album.state';
import {SampleAlbumGetter}   from '@/app/sample/album/vuex/sample-album.getter';
import {SampleAlbumMutation} from '@/app/sample/album/vuex/sample-album.mutation';
import {SampleAlbumAction}   from '@/app/sample/album/vuex/sample-album.action';

const SampleAlbumContext = new Module({
  state    : SampleAlbumState,
  getters  : SampleAlbumGetter,
  mutations: SampleAlbumMutation,
  actions  : SampleAlbumAction
});

export class SampleAlbumStore {
  static getInstance() {
    return SampleAlbumContext.context(createStore(SampleAlbumContext));
  }
}
