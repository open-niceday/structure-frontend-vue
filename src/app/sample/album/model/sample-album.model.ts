import 'reflect-metadata';
import {Expose}                                 from 'class-transformer';
import {IsInt, IsNotEmpty, IsString, MaxLength} from 'class-validator';

import {Description} from '@/core/decorator/description.decorator';

export namespace SampleAlbum {
  export namespace Request {
    export class Search {
      @Expose() @Description('userId')
      @IsInt() @IsNotEmpty()
      userId!: number;

      @Expose() @Description('title')
      @IsString() @IsNotEmpty() @MaxLength(200)
      title!: string;
    }

    export class Add {
      @Expose() @Description('userId')
      @IsInt() @IsNotEmpty()
      userId!: number;

      @Expose() @Description('title')
      @IsString() @IsNotEmpty() @MaxLength(200)
      title!: string;
    }

    export class Modify {
      @Expose() @Description('id')
      @IsInt() @IsNotEmpty()
      id!: number;

      @Expose() @Description('userId')
      @IsInt() @IsNotEmpty()
      userId!: number;

      @Expose() @Description('title')
      @IsString() @IsNotEmpty() @MaxLength(200)
      title!: string;
    }
  }

  export namespace Response {
    export class FindAll {
      @Expose() @Description('id')
      id!: number;

      @Expose() @Description('userId')
      userId!: number;

      @Expose() @Description('title')
      title!: string;
    }

    export class FindOne {
      @Expose() @Description('id')
      id!: number;

      @Expose() @Description('userId')
      userId!: number;

      @Expose() @Description('title')
      title!: string;
    }
  }
}
