# 프론트엔드 개발 기준(structure-frontend-vue)
  * structure-frontend-vue 개발을 위한 기본 개발환경을 제공한다.  


# 이력
  * v0.0.1          
    * 2020.02.03 
      * 최초 등록

# 구성
  <!-- blank line -->
  * 환경
    * windows 10, mac
    * webstorm 2019.3.2
    * node v12.14.2
    * npm v6.13.4
    * vue-cli v4.1.2

  <!-- blank line -->
  * 라이브러리
    * vue-class-component
      * https://github.com/vuejs/vue-class-component
    * class-validator
      * https://github.com/typestack/class-validator
    * class-transformer
      * https://github.com/typestack/class-transformer
    * vuex-smart-module
      * https://github.com/ktsn/vuex-smart-module
    * vue-property-decorator
      * https://github.com/kaorun343/vue-property-decorator
    * axiosService
      * https://github.com/axiosService/axiosService
    * lodash
      * https://lodash.com
    * qs
      * https://github.com/ljharb/qs
     

# 설치 및 실행
<!-- blank line -->
  * 설치
    * Node 모듈 설치 (<a href="https://nodejs.org/ko/">Link</a>)
    * Git 설치 (<a href="https://git-scm.com/">Link</a>)
    * vue-cli 설치 (<a href="https://cli.vuejs.org/">Link</a>)
    * Webstrom 설치 (<a href="https://www.jetbrains.com/ko-kr/webstorm/download/#section=windows">Link</a>)
  
  * 실행
    * npm install
    * npm run serve
    * npm run build

  ## WebStorm
  * 코드 spaces 셋팅
    * Preferences > Editor > Code Style
    * HTML > Tabs and Indents 
      * Tab Size : 2
      * Indent : 2
      * Continuation Indent: 4
    * Typescript > Tabs and Indents 
      * Tab Size : 2
      * Indent : 2
      * Continuation Indent: 2
    * Typescript > Punctuation
      * use "Single" quotes "in new Code"
  * Key Mapping 셋팅
    * "Reformat Code" 항목 단축키 등록
  
# 개발
<!-- blank line -->
  ## 패키지 및 파일 명명 규칙
  * 모든 화면 파일명(*.vue)은 "kebab-case"로 작성한다. 
    * Good: sample-list.vue
    * Bad: SampleList.vue
  * 모든 화면 파일명(*.vue)은 기능의 해당하는 두가지 단어 이상으로 작성한다. 
    * Good: sample-list.vue
    * Bad: sample.vue
  * 모든 기능 파일명(*.ts)은 "kebab-case" + "dot.case"로 작성한다.
    * Good: platform.service.ts
    * Bad: platform-service.ts
    * 기능 ex) service, model, router, service...
  * 모든 폴더는 "kebab-case"로 작성한다.
    * Good: platform, tv-platform
    * Bad: TvPlatform, tvPlatform
  * 기타 파일은 "kebab-case"로 작성한다.
    * Good: style.css, default-style.css
    * Bad: defaultStyle.css
  
  ## 패키지 구조
  ```
  ├── public
  │   ├── favicon.icon
  │   └── index.html
  ├── node_modules
  ├── src
  │   ├── app
  │   │   ├── {{대분류}}
  │   │   │   └── {{중분류}}
  │   │   │       └── {{소분류}}
  │   │   │           ├── components
  │   │   │           ├── model
  │   │   │           ├── router
  │   │   │           ├── service
  │   │   │           ├── view
  │   │   │           └── vuex
  │   │   │               ├── action
  │   │   │               ├── getter
  │   │   │               ├── mutation
  │   │   │               ├── state
  │   │   │               └── store
  │   │   ├── sample
  │   │   └── shared
  │   │       ├── components
  │   │       ├── layer
  │   │       ├── layout
  │   │       ├── layout
  │   │       ├── model
  │   │       ├── not-found
  │   │       ├── router
  │   │       └── service
  │   ├── assets
  │   │   ├── css
  │   │   ├── font
  │   │   └── images
  │   │       ├── common
  │   │       └── icon
  │   ├── modules
  │   │   └── @types
  │   │       └── @{{명칭}}
  │   ├── core
  │   │   ├── decorator
  │   │   ├── event
  │   │   ├── model
  │   │   ├── service
  │   │   └── index.ts
  │   ├── app.vue
  │   ├── main.ts
  │   ├── router.ts
  │   └── shims-vue.d.ts
  ├── .gitignore
  ├── babel.config.js
  ├── env.development
  ├── env.product
  ├── package.json
  ├── postcss.config.js
  ├── readme.md
  ├── tsconfig.json
  ├── tslint.json
  └── vue.config.js
  ```
  ## 상위 패키지 구조 설명   
  | 패키지                                 | 설명 |
  | --------                              |  -------- |
  | /node_modules                         | NPM 라이브러리 영역 |
  | /public                               | 최상위 html 영역 |
  | /src/app                              | 화면 주요구성요소 구현영역 |
  | /src/app/shared                       | 화면 주요구성요서 공통영역 구현영역 |
  | /assets                               | 화면 구성을 보조하는 영역 |
  | /core                                 | 프로젝트 기본 구성요소 영역 |
  | /modules                              | 프로젝트 추가 라이브러리 보조영역 |
  | /publish                              | 퍼블리싱 구성영역 |
    
  ## 패키지 구조 설명   
  | 패키지                                 | 설명 |
  | --------                              |  -------- |
  | /src/app/화면구성영역                    | 화면 영역 |
  | /src/app/sample                       | 셈플페이지 구성 영역 |
  | /src/app/shared/components            | 화면 공통 component 구성 영역 |
  | /src/app/shared/layer                 | 화면 공통 layer 구성 영역 |
  | /src/app/shared/layout                | 화면 공통 layout 구성 영역 |
  | /src/app/shared/model                 | 화면 공통 model 구성 영역 |
  | /src/app/shared/not-found             | not found 화면 구성 영역 |
  | /src/app/shared/router                | 화면 공통 router 구성 영역 |
  | /src/app/shared/service               | 화면 공통 service 구성 영역 |
  | /src/assets/css                       | 전체 css 구성 영역 |
  | /src/assets/font                      | 전체 font 구성 영역 |
  | /src/assets/images                    | 전체 images 구성 영역 |
  | /src/modules/@types                   | 라이브러리 typescript index 구성 영역 |
  | /src/core/decorator                   | 전역 decorator 구성 영역 |
  | /src/core/event                       | 전역 event 구성 영역 |
  | /src/core/model                       | 전역 model 구성 영역 |
  | /src/core/service                     | 전역 service 구성 영역 |
  
  ## 기능별 패키지 구조
  | Package                              | Module             | URL                                 | Menu |
  | --------                             | --------           | --------                            | -------- |               
  | platform.common.frame                | frame              | /platform/common/frame              | 방송플렛폼 > 공통 > 프레임 |					
  | platform.program                     | program            | /platform/program                   | 방송플렛폼 > 프로그램 |					
  | platform.template                    | template           | /platform/template                  | 방송플렛폼 > 탬플릿 |					
  | platform.page                        | page               | /platform/page                      | 방송플렛폼 > 페이지 |					
  | platform.layout.background.color     | background-color   | /platform/layout/background/color   | 방송플렛폼 > 레이아웃 > 백그라운드 > 컬러 |					
  | platform.layout.background.image     | background-image   | /platform/layout/background/image   | 방송플렛폼 > 레이아웃 > 백그라운드 > 이미지 |					
  | platform.layout.font                 | font               | /platform/layout/font               | 방송플렛폼 > 레이아웃 > 폰트 |					
  | platform.component.menu              | menu               | /platform/component/menu            | 방송플렛폼 > 컴포넌트 > 메뉴 |					
  | platform.component.album             | album              | /platform/component/album           | 방송플렛폼 > 컴포넌트 > 앨범 |					
  | platform.component.song.general      | song-general       | /platform/component/song/general    | 방송플렛폼 > 컴포넌트 > 음원 > 기본 |					
  | platform.component.song.album        | song-album         | /platform/component/song/album      | 방송플렛폼 > 컴포넌트 > 음원 > 앨범/음원 |					
  | platform.component.movie.general     | movie-general      | /platform/component/movie/general   | 방송플렛폼 > 컴포넌트 > 영상 > 기본 |					
  | platform.component.movie.main        | movie-main         | /platform/component/movie/main      | 방송플렛폼 > 컴포넌트 > 영상 > 메인/서브 |					
  | platform.component.main.general      | main-general       | /platform/component/main/general    | 방송플렛폼 > 컴포넌트 > 메인이미지 > 기본 |					
  | platform.component.main.slide        | main-slide         | /platform/component/main/slide      | 방송플렛폼 > 컴포넌트 > 메인이미지 > 슬라이드 |					
  | platform.component.image.general     | image-general      | /platform/component/image/general   | 방송플렛폼 > 컴포넌트 > 이미지 > 기본 |					
  | platform.component.image.position	 | image-position     | /platform/component/image/position  | 방송플렛폼 > 컴포넌트 > 이미지 > 노출/이미지 |					
  | platform.component.lineup            | lineup             | /platform/component/lineup          | 방송플렛폼 > 컴포넌트 > 라인업 |					
  | platform.component.comment           | comment            | /platform/component/comment         | 방송플렛폼 > 컴포넌트 > 댓글 |					
  | platform.component.text              | text               | /platform/component/text            | 방송플렛폼 > 컴포넌트 > 텍스트 |					
  | platform.component.photo             | photo              | /platform/component/photo           | 방송플렛폼 > 컴포넌트 > 포토 |					
  | platform.component.board.general     | board-general      | /platform/component/board/general   | 방송플렛폼 > 컴포넌트 > 게시판 > 일반 |					
  | platform.component.board.manage      | board-manage       | /platform/component/board/manage    | 방송플렛폼 > 컴포넌트 > 게시판 > 운영 |					
  | platform.component.tab               | tab                | /platform/component/tab             | 방송플렛폼 > 컴포넌트 > 탭 |					
  | platform.component.popup             | popup              | /platform/component/popup           | 방송플렛폼 > 컴포넌트 > 팝업 |					
  | platform.operate.board.general       | board-general      | /platform/operate/board/general     | 방송플렛폼 > 관리 > 게시판 > 일반 |					
  | platform.operate.board.manage        | board-manage       | /platform/operate/board/manage      | 방송플렛폼 > 관리 > 게시판 > 운영 |					
  | platform.operate.comment             | comment            | /platform/operate/comment           | 방송플렛폼 > 관리 > 댓글 |					

  ## 화면 별 패키지 구조
  ```
  ├── components
  ├── model
  ├── router
  ├── service
  ├── view
  └── vuex
      ├── action
      ├── getter
      ├── mutatuion
      ├── state
      └── store
  ```
 
  ## 화면 별 패키지 구조 설명
  | 패키지          | 설명 |
  |  --------      |  -------- |
  | /components    | 컴포넌트 영역 |
  | /model         | 모델 영역 |
  | /router        | 라우터 영역 |
  | /service       | 서비스 영역 |
  | /view          | 화면 영역 |
  | /vuex/action   | vuex action 영역 |
  | /vuex/getter   | vuex getter 영역 |
  | /vuex/mutation | vuex mutation 영역 |
  | /vuex/state    | vuex state 영역 |
  | /vuex/store    | vuex store 영역 |
  
# 코딩 명명 규칙
  ## 함수명
  | 함수명          | 설명 |
  |  --------      |  -------- |
  | onAdd          | 등록 페이지 이동 |
  | onModify       | 수정 페이지 이동 |
  | onCancel       | 이전 페이지 이동 |
  | onRow          | 목록 테이블 ROW 데이터 클릭 |
  | onSave         | 등록 FORM Action |
  | onUpdate       | 수정 FORM Action |
  | onDelete       | 삭제 FORM Action |
  | onSearch       | 검색 FORM Action |
  
  ## Action
  | Action 명       | 설명 |
  |  --------       |  -------- |
  | getAll          | 목록 조회 |
  | getOne          | 상세 조회 |
  | getPage         | 목록 페이지 조회 |
  | add             | 등록 |
  | update          | 수정 |
  | delete          | 삭제 |
    
  ## 접근제어자
  * 모든 public 접근제어자는 생략한다.
  * 화면에서 사용하는 변수, 함수는 public 이다.
  * class 내부적으로 사용하는 함수, 변수는 private를 명시적으로 선언한다.
  
  ## 명명규칙
  | 단위            | 설명 |
  |  --------      |  -------- |
  | variable       | camelCase |
  | function       | camelCase |
  | class          | PascalCase |
  | folder         | kebab-case |
  | *.ts           | kebab-case + dot.case + .ts |
  | *.css          | kebab-case + .* |
  | *.vue          | kebab-case + .vue |
  | *.other        | kebab-case + .* |

# 참조
  ## 패키지 구조
  * Vue webpack package
    * http://vuejs-templates.github.io/webpack/
  * Vue.js 2 + Vuex + Router + yarn!
    * https://medium.com/tldr-tech/vue-js-2-vuex-router-yarn-basic-configuration-version-2-7b9c489d43b3
  * How to Structure a Vue.js Project
    * https://itnext.io/how-to-structure-a-vue-js-project-29e4ddc1aeeb
  * vuejs-component-style-guide
    * https://github.com/pablohpsilva/vuejs-component-style-guide
